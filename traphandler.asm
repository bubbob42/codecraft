* traphandler.asm - Example trap handling code (leaves D0 intact).  Entered
* in supervisor mode with the following on the supervisor stack:
*    0(sp).l = trap#
*    4(sp) Processor dependent exception frame

    INCLUDE "exec/types.i"
    INCLUDE "lvo/exec_lib.i"
    
    section text,CODE

    xdef @trapa
    xdef @loop
    xref _brokenAddress
    xref _brokenSR
    xref _brokenRegs
    xref _opcodeStepping
    xref @BreakHandler


@trapa                                 ; our trap handler entry
        ADDQ     #4,SP                   ; remove exception number from SSP

        MOVEM.L  a0/d0,-(sp)
                
        LEA      _brokenAddress,a0
        MOVE.L   (a0),d0
        BEQ      gotobroken
resume
        MOVE.L   d0,10(SP)
        MOVE.L   #0,(a0)
        LEA      @loop,a0               ; restore loop opcode
        MOVE.W   #$60FE,(a0)
        LEA      _brokenSR,a0            ; restore SR
        MOVE.W   (a0),8(SP)
        LEA      _opcodeStepping,a0
        TST.L    (a0)
        BEQ      nostepping
        BSET     #15,8(SP)
        
nostepping
        MOVEM.L  (sp)+,a0/d0
        RTE
        
gotobroken
        MOVE.L   10(SP),(a0)            ;brokenAdress = PC
        LEA      idlecode,a0            ; PC = idlecode
        MOVE.L   a0,10(SP)
        BCLR     #15,8(SP)              ; no opcodestepping here
        LEA      _brokenSR,a0            ; save SR for dbrg and resume
        MOVE.W   8(SP),(a0)
        MOVEM.L  (sp)+,a0/d0
        RTE                             ; return from exception


idlecode
        MOVEM.L  a0-a7/d0-d7,-(sp)
        LEA      _brokenRegs,a0            ; save SR for dbrg and resume
        MOVE.L   sp,d0
        ADDQ.L   #4,d0
        MOVE.L   d0,(a0)
        JSR      @BreakHandler
        MOVEM.L  (sp)+,a0-a7/d0-d7

@loop
        BRA     @loop            
        END
