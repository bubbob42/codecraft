cp userguide-reference.docx DiskTemplate
cd DiskTemplate
pandoc -t docx userguide.md --reference-doc=userguide-reference.docx -o userguide.docx
mv userguide.docx ..
rm userguide-reference.docx
cd ..
