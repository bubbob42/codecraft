/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>

#include <intuition/classusr.h>
#include <gadgets/radiobutton.h>
#include <gadgets/checkbox.h>
#include <gadgets/listbrowser.h>
#include <gadgets/layout.h>
#include <gadgets/palette.h>
#include <gadgets/texteditor.h>
#include <gadgets/clicktab.h>
#include <gadgets/string.h>
#include <classes/window.h>
#include <images/label.h>
#include <images/penmap.h>
#include <libraries/iffparse.h>
#include <libraries/gadtools.h>
#include <dos/dos.h>
#include <dos/exall.h>
#include <dos/dostags.h>

#include <proto/exec.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/utility.h>
#include <proto/asl.h>
#include <proto/texteditor.h>
#include <proto/locale.h>
#include <proto/radiobutton.h>
#include <proto/checkbox.h>
#include <proto/listbrowser.h>
#include <proto/getfile.h>
#include <proto/label.h>
#include <proto/penmap.h>
#include <proto/layout.h>
#include <proto/window.h>
#include <proto/clicktab.h>
#include <proto/string.h>
#include <proto/palette.h>
#include <proto/button.h>
#include <proto/iffparse.h>
#include <proto/gadtools.h>
#include <proto/dos.h>

#include <tools/textedit/extension.h>
#include <tools/textedit/plugin.h>
#include "breakpoints.h"
#include "codecraft.h"
#include "debugger.h"
#include "codecraft_rev.h"

#define CATCOMP_NUMBERS;
#include "strings.h"
const UBYTE *GetStr(LONG stringNum);

extern struct Library *TextFieldBase;
extern struct Library *UtilityBase;
extern struct Locale *Locale;
extern struct Library *CheckBoxBase;
extern struct Library *IFFParseBase;
extern struct Library *WindowBase;
extern struct Library *StringBase;
extern struct Catalog *Catalog;
extern struct Library *AslBase;
extern struct Library *GadToolsBase;

void ensureOpenLibs(void);
void ensureCloseLibs(void);

static void __ASM__ init(__REG__(a0, APTR userData));
void disposeGadgets(struct Extension *ext);
void processGadgetUp(struct Extension *ext, UWORD gadgetId, struct Window *window);

BOOL __ASM__ handlekey(__REG__(a0, struct Extension *ext), __REG__(a1, struct IntuiMessage *msg));
void InitMenus(struct Codecraft *ads);

struct CodecraftDoc *extractBreakpointDocument(struct Codecraft *ads, APTR document);


#define INT_VERSION    1
#define INT_REVISION   0

#define MAXPATHLEN 256

struct Plugin extensionPlugin =
{
  0,
  EXTENSIONPLUGIN_VERSION,
  MASTER_VERSION,
  INT_VERSION,
  INT_REVISION,
  0,
  0,
  &init
};

struct Codecraft myCodecraft;


void disposeCodecraftDoc(struct CodecraftDoc *adsDoc)
{
	struct BreakPoint *breakPoint;

	if (!adsDoc)
		return;
		
	while (breakPoint = (struct BreakPoint *)RemTail((struct List *)&adsDoc->breakPointList))
	{
		Remove(breakPoint->browserNode);
		FreeListBrowserNode(breakPoint->browserNode);
		FreeVec(breakPoint);
	}
	FreeMem(adsDoc, sizeof(struct CodecraftDoc));
}

static void __ASM__ terminate(__REG__(a0, struct Extension *ext))
{
	UWORD i;
	struct Codecraft *ads = &myCodecraft;
	struct CodecraftDoc *adsDoc;

	while(adsDoc = (struct CodecraftDoc *)RemTail((struct List *)&ads->breakpointDocList))
		disposeCodecraftDoc(adsDoc);

	if (ads->projectDirLock)
	{
		struct Node *node;
		struct LinkedNotifyRequest *notify;

		UnLock(ads->projectDirLock);
		ads->projectDirLock = NULL;
		
		while (notify = (struct LinkedNotifyRequest *)RemHead((struct List *)&ads->notifyRequestList))
		{
			EndNotify(&notify->nr);
			FreeMem(notify, sizeof(struct LinkedNotifyRequest));
		}
			
		/* clear any old tree that was loaded */
		while (node = RemHead(&ads->projectTreeList))
			FreeListBrowserNode(node);
	}

	FreeMenus((struct Menu *)ads->recentMenuItems);
	FreeMenus(ads->settingsmenu);
	FreeMenus(ads->menu);

	if (ads->visualInfo)
		FreeVisualInfo(ads->visualInfo);

	if (ads->window)
		ReleasePen(ads->CMap, ads->variableBrowserAltPen);

	for (i = 0; i < BPIMG_COUNT; i++)
		if (ads->Images[i])
			DisposeObject(ads->Images[i]);

    if (myCodecraft.pipeFh)
		Close(ads->pipeFh);

    if (myCodecraft.asyncFilePort)
		DeleteMsgPort(ads->asyncFilePort);

    if (myCodecraft.FileRequester)
		FreeAslRequest(ads->FileRequester);

	if (ads->projectDirLock)
		UnLock(ads->projectDirLock);

	ensureCloseLibs();
}

void createWelcomeGadget(struct Codecraft *ads)
{
	ads->welcomeGadget = NewObject(NULL, "button.gadget",
		BUTTON_BevelStyle, BVS_NONE,
		GA_ReadOnly, TRUE,
		GA_Text, "Welcome to Codecraft",
	TAG_DONE );
}


ULONG labels[] = {MSG_UTILTAB_BUILDLOG, MSG_UTILTAB_BREAKPOINTS, MSG_UTILTAB_VARIABLES, MSG_UTILTAB_CALLSTACK, NULL};

void createUtilityClickTab(struct Codecraft *ads)
{
	ULONG *id = labels;
	
	ads->utilityPageGroup = NewObject(PAGE_GetClass(), NULL,
				LAYOUT_SpaceInner, FALSE,
				LAYOUT_SpaceOuter, FALSE,
				TAG_END);

	NewList(&ads->utilityTabs);
	
	while (*id)
	{
		struct Node *node;
		
		node = AllocClickTabNode(
			TNA_Text, GetStr(*id),
			TAG_END);
		
		AddTail(&ads->utilityTabs, node);
		id++;
	}
		
	ads->utilityClickTab = NewObject(CLICKTAB_GetClass(), NULL,
				CLICKTAB_PageGroupBorder, FALSE,
                CLICKTAB_AutoTabNumbering, TRUE,
				GA_Underscore, NULL,
				CLICKTAB_Labels, &ads->utilityTabs,
				CLICKTAB_PageGroup, ads->utilityPageGroup,
				TAG_DONE);
}

void createBuildBrowser(struct Codecraft *ads)
{
	NewList(&ads->msgNodeList);

	ads->buildBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
				GA_ID, GLOBALGID_BUILDBROWSER,
				GA_RelVerify, TRUE,
				LISTBROWSER_ShowSelected, TRUE,
				LISTBROWSER_MultiSelect, FALSE,
				LISTBROWSER_WrapText, TRUE,
				TAG_DONE);

	SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
				PAGE_Add, ads->buildBrowser,
				TAG_END);
}

void createBreakpointBrowser(struct Codecraft *ads)
{
	struct ColumnInfo *ci;

	NewList(&ads->breakpointBrowserList);

	ci = AllocLBColumnInfo(3,
				LBCIA_Column, 0,
					LBCIA_Title, "",
					LBCIA_Flags, CIF_NOSEPARATORS,
					LBCIA_Width, 15,
				LBCIA_Column, 1,
					LBCIA_Title, "",
					LBCIA_Flags, CIF_NOSEPARATORS,
					LBCIA_Width, 14,
				LBCIA_Column, 2,
					LBCIA_Title, "Location",
					LBCIA_Flags, CIF_NOSEPARATORS,
				TAG_DONE);

	ads->breakpointBrowser = NewObject(LISTBROWSER_GetClass(), NULL,
				GA_ID, GLOBALGID_BREAKPOINTBROWSER,
				GA_RelVerify, TRUE,
				LISTBROWSER_ShowSelected, TRUE,
				LISTBROWSER_MultiSelect, FALSE,
				LISTBROWSER_Separators, FALSE,
				LISTBROWSER_ColumnInfo, (ULONG)ci,
				TAG_DONE);

	SetGadgetAttrs((struct Gadget *)ads->utilityPageGroup, ads->window, NULL,
				PAGE_Add, ads->breakpointBrowser,
				TAG_END);
}

void createMakefileBrowser(struct Codecraft *ads)
{
    NewList(&ads->projectTreeList);

	ads->makefileExpGadget = NewObject( NULL, "button.gadget",
			GA_ID, GLOBALGID_SIDEBARRESIZER,
			GA_RelVerify, TRUE,
			GA_Text, "<",
			TAG_END);
				
	ads->makefileListGadget = NewObject(LISTBROWSER_GetClass(), NULL,
			GA_ID, GLOBALGID_MAKEFILEBROWSER,
            GA_RelVerify, TRUE,
            LISTBROWSER_ShowSelected, TRUE,
            LISTBROWSER_MultiSelect, FALSE,
                LISTBROWSER_Hierarchical, TRUE,
			TAG_DONE);

    ads->makefileGadget = NewObject(LAYOUT_GetClass(), NULL,
            LAYOUT_Orientation, LAYOUT_ORIENT_VERT,
            LAYOUT_SpaceInner, TRUE,
            LAYOUT_SpaceOuter, FALSE,
			LAYOUT_AddChild, ads->makefileExpGadget,
			CHILD_WeightedHeight, 0,
            LAYOUT_AddChild, ads->makefileListGadget,
			CHILD_NoDispose, TRUE,
            CHILD_WeightedHeight, 100,
			TAG_DONE);

	ads->makefileGadWeight = 0;
}

void ensureOpenLibs(void)
{
	SysBase  = (*((struct ExecBase **) 4));

	if (! (DOSBase = (struct DosLibrary *)OpenLibrary("dos.library",47)))
        return;

	if (! (IntuitionBase = (struct IntuitionBase *)OpenLibrary("intuition.library",47)))
        return;

	if (! (GfxBase = (struct GfxBase *)OpenLibrary("graphics.library",47)))
        return;

    if (! (UtilityBase = OpenLibrary("utility.library",47)))
        return;

    if (! (LayoutBase = OpenLibrary("gadgets/layout.gadget",47)))
        return;

    if (! (TextFieldBase = OpenLibrary("gadgets/texteditor.gadget",47)))
        return;

    if (! (ListBrowserBase = OpenLibrary("gadgets/listbrowser.gadget",47)))
        return;

    if (! (RadioButtonBase = OpenLibrary("gadgets/radiobutton.gadget",47)))
        return;

	if (! (CheckBoxBase = OpenLibrary("gadgets/checkbox.gadget",47)))
		return;

	if (! (StringBase = OpenLibrary("gadgets/string.gadget",47)))
		return;

	if (! (GetFileBase = OpenLibrary("gadgets/getfile.gadget",47)))
		return;

	if (! (ClickTabBase = OpenLibrary("gadgets/clicktab.gadget",47)))
		return;

	if (! (WindowBase = OpenLibrary("window.class",47)))
		return;

	if (! (LabelBase = OpenLibrary("images/label.image",47)))
        return;

    if (! (PenMapBase = OpenLibrary("images/penmap.image",47)))
        return;

    if (! (IFFParseBase = OpenLibrary("iffparse.library",45)))
        return;

    if (! (AslBase = OpenLibrary("asl.library",45)))
        return;

    if (! (GadToolsBase = OpenLibrary("gadtools.library",47)))
        return;

	if (LocaleBase = (struct LocaleBase *)OpenLibrary("locale.library",39)) {
		Catalog = OpenCatalog(NULL,"codecraft.catalog",TAG_DONE);
        Locale = OpenLocale(NULL);
    }
}

void ensureCloseLibs(void)
{
	if (ListBrowserBase)
		CloseLibrary(ListBrowserBase);

	if (StringBase)
		CloseLibrary(StringBase);

	if (GetFileBase)
		CloseLibrary(GetFileBase);

	if (CheckBoxBase)
		CloseLibrary(CheckBoxBase);

	if (RadioButtonBase)
		CloseLibrary(RadioButtonBase);

	if (ClickTabBase)
		CloseLibrary(ClickTabBase);

	if (WindowBase)
		CloseLibrary(WindowBase);

	if (PenMapBase)
		CloseLibrary(PenMapBase);

    if (GadToolsBase)
        CloseLibrary(GadToolsBase);

    if (IFFParseBase)
        CloseLibrary(IFFParseBase);

    if (AslBase)
        CloseLibrary(AslBase);

    if (LocaleBase) {
        CloseCatalog(Catalog);
        CloseLibrary(LocaleBase);
    }

    if (TextFieldBase)
        CloseLibrary(TextFieldBase);

    if (GfxBase)
        CloseLibrary(GfxBase);

    if (IntuitionBase)
        CloseLibrary(IntuitionBase);

    if (DOSBase)
        CloseLibrary(DOSBase);
}

STRPTR buildPathFromTree(struct Node *startNode, STRPTR buffer)
{
    ULONG startGen;
    struct Node *node;
    STRPTR nodeText;

    GetListBrowserNodeAttrs(startNode,
        LBNA_Generation, &startGen,
        LBNA_Column, 0,
        LBNCA_Text, &nodeText,
        TAG_DONE
    );

    if (startGen < 1)
        return NULL;
    if (startGen == 1)
    {
        SNPrintf(buffer, MAXPATHLEN, "%s", nodeText);
        return buffer+strlen(buffer);
    }

    node = startNode->ln_Pred;
    while (node->ln_Pred)
    {
        ULONG nodeGen;

        GetListBrowserNodeAttrs(node,
            LBNA_Generation, &nodeGen,
            TAG_DONE
        );

        if (nodeGen == (startGen -1))
        {
            buffer = buildPathFromTree(node, buffer);
            SNPrintf(buffer, MAXPATHLEN, startGen > 1 ? "/%s": "%s", nodeText);
            return buffer + strlen(buffer);
        }
        node = node->ln_Pred;
    }
    return NULL;
}

BOOL __ASM__ handleGadgetUp(__REG__(a0, struct Extension *ext), __REG__(d0, UWORD gadgetId))
{
    struct Codecraft *ads = &myCodecraft;

	switch (gadgetId)
	{
	case GLOBALGID_MAKEFILEBROWSER:
    {
        ULONG relevent;
        struct Node *node;
        char buffer[MAXPATHLEN];

        GetAttr(LISTBROWSER_SelectedNode, ads->makefileListGadget, (ULONG *)&node);
        GetAttr(LISTBROWSER_RelEvent, ads->makefileListGadget, &relevent);

		if (node && relevent == LBRE_DOUBLECLICK)
        {
			Strncpy(buffer, "OPEN FILENAME \"", MAXPATHLEN);
			NameFromLock(ads->projectDirLock, buffer+15, MAXPATHLEN-15);
            Strncat(buffer, "/", MAXPATHLEN);
            buildPathFromTree(node, buffer + strlen(buffer));
			Strncat(buffer, "\"", MAXPATHLEN);
			ads->tei->executeRexxCommand(NULL, buffer);
        }
        return TRUE;
    }
	case GLOBALGID_BUILDBROWSER:
	{
		ULONG relevent;
		struct Node *node;
		STRPTR text;

		GetAttr(LISTBROWSER_SelectedNode, ads->buildBrowser, (ULONG *)&node);
		GetAttr(LISTBROWSER_RelEvent, ads->buildBrowser, &relevent);

		if (relevent == LBRE_DOUBLECLICK)
		{
			GetListBrowserNodeAttrs(node,
				LBNCA_Text, (ULONG *)&text,
				TAG_DONE);
			parseBuildLine(ads, text);
		}

		return TRUE;
	}
	case GLOBALGID_BREAKPOINTBROWSER:
	{
		ULONG relevent;
		struct Node *node;
		struct BreakPoint *breakPoint;

		GetAttr(LISTBROWSER_SelectedNode, ads->breakpointBrowser, (ULONG *)&node);
		GetAttr(LISTBROWSER_RelEvent, ads->breakpointBrowser, &relevent);

		if (relevent == LBRE_DOUBLECLICK)
		{
			GetListBrowserNodeAttrs(node,
				LBNA_UserData, (ULONG *)&breakPoint,
				TAG_DONE);
			revealBreakPoint(breakPoint);
		} else if (relevent == LBRE_CHECKED)
			setDisabledBreakPoint(ads, node, FALSE);
		else if (relevent == LBRE_UNCHECKED)
			setDisabledBreakPoint(ads, node, TRUE);
			
	    ads->tei->redrawLeftBar(ads->currentAdsDocument->document);

		return TRUE;
	}
	case GLOBALGID_VARIABLEBROWSER:
	{
		onVariableBrowserGadgetUp(ads);
		return TRUE;
	}
	case GLOBALGID_CALLSTACKBROWSER:
	{
		onCallStackBrowserGadgetUp(ads);
		return TRUE;
	}
	case GLOBALGID_SIDEBARRESIZER:
	{
		if (ads->makefileGadWeight)
		{
			SetGadgetAttrs((struct Gadget *)ads->makefileExpGadget, ads->window, NULL,
					GA_Text, "<",
					TAG_END);
			SetGadgetAttrs((struct Gadget *)ads->makefileGadget, ads->window, NULL,
					LAYOUT_ModifyChild, ads->makefileExpGadget,
					CHILD_WeightedHeight, 0,
					LAYOUT_AddChild, ads->makefileListGadget,
					CHILD_NoDispose, TRUE,
					TAG_END);
			ads->tei->resizeEdgeGadget(TEEdge_Left, ads->makefileGadget, ads->makefileGadWeight);
			ads->makefileGadWeight = 0;
			SetGadgetAttrs((struct Gadget *)ads->makefileListGadget, ads->window, NULL,
				GA_Hidden, FALSE,
				TAG_END);
		}
		else
		{
			ads->makefileGadWeight = ads->tei->weightOfEdgeGadget(TEEdge_Left, ads->makefileGadget);
			SetGadgetAttrs((struct Gadget *)ads->makefileExpGadget, ads->window, NULL,
					GA_Text, ">",
					TAG_END);
			SetGadgetAttrs((struct Gadget *)ads->makefileGadget, ads->window, NULL,
					LAYOUT_RemoveChild, ads->makefileListGadget,
					LAYOUT_ModifyChild, ads->makefileExpGadget,
					CHILD_WeightedHeight, 100,
					TAG_END);
			ads->tei->resizeEdgeGadget(TEEdge_Left, ads->makefileGadget, 0);
			SetGadgetAttrs((struct Gadget *)ads->makefileListGadget, ads->window, NULL,
				GA_Hidden, TRUE,
				TAG_END);
		}
		return TRUE;
	}
	}
    return FALSE;
}

#define IMGWIDTH 13
#define IMGHEIGHT 9
struct {ULONG  wh; char pens[(IMGWIDTH+2)*IMGHEIGHT];} buttonPens[BPIMG_COUNT] = {
{ 0,
	"             "
	"   2222      "
	" 22333322    "
	"2333333332   "
	"2333333332   "
	"2333333332   "
	" 22333322    "
	"   2222      "
	"             "
},
{ 0,
	"             "
	"    2        "
	"  22322      "
	" 2333332     "
	"233333332    "
	" 2333332     "
	"  22322      "
	"    2        "
	"             "
},
{ 0,
	"             "
	"   3333      "
	" 33222233    "
	"3222222223   "
	"3222222223   "
	"3222222223   "
	" 33222233    "
	"   3333      "
	"             "
},
{ 0,
	"             "
	"    3        "
	"  33233      "
	" 3222223     "
	"322222223    "
	" 3222223     "
	"  33233      "
	"    3        "
	"             "
},
{ 0,
	"      4      "
	"      44     "
	"      444    "
	"4444444444   "
	"4444444444411"
	"444444144411 "
	" 1111144411  "
	"      4411   "
	"      411    "
},
{ 0,
	"             "
	"  111        "
	"  141  111   "
	"  144111441  "
	"   144444441 "
	"    1111441  "
	"       111   "
	"             "
	"             "
}
};

ULONG palette[] =
{
    5,
    0, 0, 0,
    0, 0, 0,
    0xffffffff, 0xffffffff, 0xffffffff,
    0xffffffff, 0, 0,
    0, 0, 0xffffffff,
};

void convertFromChars(void)
{
	int i,j;
	
	for (i = 0; i< BPIMG_COUNT; i++)
	{
		char *str = buttonPens[i].pens;
		for (j = 0; j <IMGWIDTH*IMGHEIGHT; ++j)
		{
			if (str[j] == ' ')
				str[j] = 0;
			else
				str[j] -= '0';
		}
		buttonPens[i].wh = (IMGWIDTH<<16)|IMGHEIGHT;
	}
}

LONG askUser(struct Window *win, ULONG gadsMsg, ULONG questionMsg, STRPTR optStr)
{
	struct EasyStruct easy = {
	    sizeof (struct EasyStruct),
		0,
	    "Codecraft",
	};

	easy.es_TextFormat = GetStr(questionMsg);
	easy.es_GadgetFormat = GetStr(gadsMsg);
	return	EasyRequest(win, &easy, NULL, (ULONG *)optStr);
}


ULONG __SAVE_DS__ __ASM__ leftBarEntry(__REG__(a0, struct Hook *h), __REG__(a2, VOID *o), __REG__(a1, VOID *msg))
{
	struct LeftBarRenderMessage *lbrm = (struct LeftBarRenderMessage *)msg;
    struct CodecraftDoc *adsDoc = h->h_Data;
    struct Codecraft *ads = &myCodecraft;
    struct Debugger *dbgr = ads->debugger;

	if (lbrm->Command == LEFTBAR_RENDERCOMMAND && lbrm->LineSubNum == 0)
	{
		WORD left = lbrm->RastPort->cp_x;
		WORD top = lbrm->RastPort->cp_y;
		struct BreakPoint *bp;

		bp = BreakPointsFindLessOrEqual(adsDoc, NULL, lbrm->BlockNum);

		if (bp != (struct BreakPoint *)&adsDoc->breakPointList && bp->blockNum == lbrm->BlockNum)
		{
			ULONG imgIndex = 0;
			if (bp->autocontinue)
				imgIndex = BPIMG_CONTINUE;
			if (bp->disabled || (ads->debugger->debuggerMode && !bp->address) || adsDoc->changed)
				imgIndex |= BPIMG_DISABLED;
			DrawImage(lbrm->RastPort, ads->Images[imgIndex], left, top);
		}
		
		if (dbgr->debuggerMode == BROKEN)
		{
			if (dbgr->brokenBlockNum == lbrm->BlockNum
			 && Stricmp(adsDoc->fullPath, dbgr->brokenFullPath)==0)
				DrawImage(lbrm->RastPort, ads->Images[BPIMG_EXEC], left+7, top);
			else if (dbgr->contextBlockNum == lbrm->BlockNum
			 && Stricmp(adsDoc->fullPath, dbgr->contextFullPath)==0)
				DrawImage(lbrm->RastPort, ads->Images[BPIMG_CONTEXT], left+7, top);
		}
	}

	if (lbrm->Command == LEFTBAR_MOUSECOMMAND)
	{
		struct LeftBarMouseMessage *lbmm = (struct LeftBarMouseMessage *)msg;
		if (lbmm->Code == SELECTUP)
			BreakPointsToggleAtBlockNum(ads->currentAdsDocument, lbmm->BlockNum);

		return 1;
	}

	return 0;
}

void __ASM__ setWindowPointer(__REG__(a0, struct Extension *ext), __REG__(a1, struct Window *window))
{
    struct Codecraft *ads = &myCodecraft;
	UWORD i;
	ULONG bg[3];
	ULONG lg[3];


	if (ads->window)
	{
		ReleasePen(ads->CMap, ads->variableBrowserAltPen);
	}
	
	ads->window = window;

	if (ads->visualInfo)
		FreeVisualInfo(ads->visualInfo);
	ads->visualInfo = NULL;

	for (i = 0; i < BPIMG_COUNT; i++)
		if (ads->Images[i])
		{
			DisposeObject(ads->Images[i]);
			ads->Images[i] = NULL;
		}

	if (!window)
		return;

	ads->CMap = ads->window->WScreen->ViewPort.ColorMap;
	ads->visualInfo = GetVisualInfoA(window->WScreen,NULL);

	GetRGB32(ads->window->WScreen->ViewPort.ColorMap, 3, 1, palette + 1+4*3);
	for (i = 0; i < BPIMG_COUNT; i++)
		ads->Images[i] = NewObject(PENMAP_GetClass(), NULL,
					IA_Data, &buttonPens[i],
                    IA_BGPen, 0,
                    IA_Height, IMGHEIGHT,
                    PENMAP_Screen, ads->window->WScreen,
                    PENMAP_MaskBlit, TRUE,
                    PENMAP_Transparent, TRUE,
                    PENMAP_Palette, palette,
                    TAG_DONE);

	GetRGB32(ads->CMap, 0, 1, bg);
	lg[0] = 194 * (bg[0] >> 24);
	lg[1] = 194 * (bg[1] >> 24);
	lg[2] = 194 * (bg[2] >> 24);
	lg[0] = lg[0] + (255-194)*255;
	lg[1] = lg[1] + (255-194)*255;
	lg[2] = lg[2] + (255-194)*255;
	lg[0] = lg[0] | (lg[0]<<16);
	lg[1] = lg[1] | (lg[1]<<16);
	lg[2] = lg[2] | (lg[2]<<16);

	ads->variableBrowserAltPen = (LONG)ObtainBestPen (
		ads->CMap,
		lg[0],
		lg[1],
		lg[2],
		OBP_FailIfBad, FALSE,
		OBP_Precision, PRECISION_IMAGE,
	TAG_DONE);
	
	resetBreakPointImages(ads);
	
	updateRecentMenu(ads);
}

void CompletionOnCharEntered(struct ChangeListener *listener, ULONG character)
{
    struct CodecraftDoc *adsDoc = ((struct ChangeListenerCodecraft *)listener)->adsDoc;
    struct TextEditInterface *tei = adsDoc->ads->tei;
    
    DebuggerOnDocHasChanged(adsDoc->ads->debugger, adsDoc,TRUE);

return; // disable for now
	if (character == '>' || character == '.' || character == 5)
    {
		tei->executeRexxCommand(adsDoc->document, "AREXXCURSOR ON");
		tei->executeRexxCommand(adsDoc->document, "COMPLETE Pre Apple PreFix Preface preferences");
		tei->executeRexxCommand(adsDoc->document, "AREXXCURSOR OFF");
	}
}

void __ASM__ onNewDocument(__REG__(a0, struct Extension *ext), __REG__(a1, APTR document))
{
	STRPTR docFullPath;
    struct Codecraft *ads = &myCodecraft;
    struct ChangeListenerCodecraft *clads;
	struct CodecraftDoc *adsDoc;
	if (!(adsDoc = extractBreakpointDocument(ads, document)))
	{
		adsDoc = (struct CodecraftDoc *)AllocMem(sizeof(struct CodecraftDoc), 0);
		NewList((struct List *)&adsDoc->breakPointList);
	}

    clads = (struct ChangeListenerCodecraft *)AllocMem(sizeof(struct ChangeListenerCodecraft), MEMF_CLEAR|MEMF_PUBLIC);

    adsDoc->document = document;
    adsDoc->ads = ads;
	adsDoc->changeListener = clads;

    AddTail((struct List *)&ads->documentList, (struct Node *)adsDoc);

    clads->listener.onCharsInserted = BreakPointsOnCharsInserted;
    clads->listener.onCharsDeleted = BreakPointsOnCharsDeleted;
    clads->listener.onCharEntered = CompletionOnCharEntered;
    clads->adsDoc = adsDoc;
    ext->tei->installChangeListener(document, (struct ChangeListener *)clads);

	adsDoc->leftBarHook.h_Entry    = (ULONG (*)()) leftBarEntry;
	adsDoc->leftBarHook.h_SubEntry = NULL;
	adsDoc->leftBarHook.h_Data = adsDoc;
	ext->tei->installLeftBarHook(document, &adsDoc->leftBarHook);
	
	docFullPath = ads->tei->executeRexxCommand(document, "GETATTR PROJECT FILENAME");
	Strncpy(adsDoc->fullPath, docFullPath, MAXPATHLEN);
	FreeVec(docFullPath);
}

void __ASM__ onCurrentDocumentChanged(__REG__(a0, struct Extension *ext), __REG__(a1, APTR document))
{
    struct Codecraft *ads = &myCodecraft;
    struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)ads->documentList.mlh_Head;

    while (adsDoc->node.mln_Succ)
    {
        if (adsDoc->document == document)
        {
            ads->currentAdsDocument = adsDoc;
			return;
        }
        adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
    }
	ads->currentAdsDocument = NULL;
}

void __ASM__ onDisposeDocument(__REG__(a0, struct Extension *ext), __REG__(a1, APTR document))
{
    struct Codecraft *ads = &myCodecraft;
    struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)ads->documentList.mlh_Head;

    while (adsDoc->node.mln_Succ)
    {
        if (adsDoc->document == document)
            break;
        adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
    }
	if (adsDoc->node.mln_Succ)
    {
		Remove((struct Node *)adsDoc);
		adsDoc->document = NULL;

//		FreeMem(adsDoc->changeListener, sizeof(struct ChangeListenerCodecraft));
		if (adsDoc->breakPointList.mlh_Head->mln_Succ && (FilePart(adsDoc->fullPath) != adsDoc->fullPath))
		{
			AddTail((struct List *)&ads->breakpointDocList, (struct Node *)adsDoc);
			return;
		}

		disposeCodecraftDoc(adsDoc);
	}
}


void __ASM__ onRenamedDocument(__REG__(a0, struct Extension *ext), __REG__(a1, APTR document))
{
    struct Codecraft *ads = &myCodecraft;
    struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)ads->documentList.mlh_Head;

	while (adsDoc->node.mln_Succ)
	{
		if (adsDoc->document == document)
			break;
		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
	}
	if (adsDoc->node.mln_Succ)
	{
		STRPTR docFullPath = ads->tei->executeRexxCommand(document, "GETATTR PROJECT FILENAME");
		Strncpy(adsDoc->fullPath, docFullPath, MAXPATHLEN);
		FreeVec(docFullPath);
	}
}

void __ASM__ onHandleSignal(__REG__(a0, struct Extension *ext), __REG__(d0, ULONG signal))
{
    struct Codecraft *ads = &myCodecraft;

	if (ads->doRecentMenu != -1)
		openRecentProjectTree(ads, ads->doRecentMenu);
		
	if (signal & (1L << ads->asyncFilePort->mp_SigBit))
		parseBuildMessages(ads);

	if (signal & (1 << ads->notifySignal) && !ads->buildInProgress)
		fillGui(ads);

	if (signal & ads->debugger->brokenSignalMask)
		DebuggerOnBroken(ads->debugger);

	projectWizardEventHandler(ads);
	projectParametersEventHandler(ads);
}

BOOL __ASM__ aboutToQuit(__REG__(a0, struct Extension *ext))
{
	if (myCodecraft.debugger->debuggerMode != NOTACTIVE)
	{
		askUser(myCodecraft.window, MSG_OK_GAD, MSG_DBGRUNS_CANTDO, NULL);
		return FALSE;
	}

	if (myCodecraft.projectDirLock)
	{
		saveSession();
		saveProjectTree(&myCodecraft);
	}
	
    return TRUE;
}

static void __ASM__ init(__REG__(a0, APTR userData))
{
    struct Extension *ext = (struct Extension *)userData;
    struct Codecraft *ads = &myCodecraft;

    ensureOpenLibs();

    ads->tei = ext->tei;
	ads->ext = ext;

    ext->terminate = terminate;
    ext->setWindowPointer = setWindowPointer;
    ext->handleRawKey = handlekey;
    ext->handleGadgetUp = handleGadgetUp;
    ext->handleSignal = onHandleSignal;
    ext->onNewDocument = onNewDocument;
    ext->onDisposeDocument = onDisposeDocument;
    ext->onCurrentDocumentChanged = onCurrentDocumentChanged;
    ext->onRenamedDocument = onRenamedDocument;
    ext->aboutToQuit = aboutToQuit;

    ads->FileRequester = AllocAslRequest(ASL_FileRequest, TAG_END);

    NewList((struct List *)&ads->documentList);
	NewList((struct List *)&ads->breakpointDocList);
	NewList((struct List *)&ads->notifyRequestList);

	convertFromChars();

	createUtilityClickTab(ads);
	createBuildBrowser(ads);
	createBreakpointBrowser(ads);
	createVariableBrowser(ads);
	createCallStackBrowser(ads);
	
	ads->tei->installEdgeGadget(TEEdge_Bottom, ads->utilityClickTab, 20);
	createMakefileBrowser(ads);
	ads->tei->installEdgeGadget(TEEdge_Left, ads->makefileGadget, 30);

	createWelcomeGadget(ads);
	ads->tei->installWelcomeGadget(ads->welcomeGadget, "Get started");
	InitMenus(ads);

	ads->tei->setWindowFormatStrings("Codecraft", "Codecraft");

	ads->tei->installAboutText(VERS" ("DATE") Licensed under LGPL");
	
	ads->asyncFilePort = CreateMsgPort();
	ads->asyncMask = (1L << ads->asyncFilePort->mp_SigBit);
	
	ext->sigMask = ads->asyncMask;
	ads->notifySignal = AllocSignal(-1);
	if (ads->notifySignal != -1)
		ext->sigMask |= 1 << ads->notifySignal; 

	ads->debugger = CreateDebugger(ads);
	ext->sigMask |= ads->debugger->brokenSignalMask;
}
