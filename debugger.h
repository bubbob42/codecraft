#include "codecraft.h"
#include "breakpoints.h"

struct SourceLineLocations
{
	struct Node node;
	TEXT filename[256];
	ULONG fileExists;
	APTR segment;
	ULONG size;
	ULONG segmentStartOffset;
	ULONG segmentEndOffset;
	struct SLLLines
	{
		ULONG blockNum;
		ULONG offset;
		ULONG spOffset;	
	} *lines;
};

struct SymbolNode
{
	struct Node node;
	UBYTE *memPtr;
	// string data follows directly after struct
};

struct DebuggerRegisters
{
	ULONG d0;
	ULONG d1;
	ULONG d2;
	ULONG d3;
	ULONG d4;
	ULONG d5;
	ULONG d6;
	ULONG d7;
	ULONG a0;
	ULONG a1;
	ULONG a2;
	ULONG a3;
	ULONG a4;
	ULONG a5;
	ULONG a6;
	ULONG a7;
};

struct Debugger
{
	struct Codecraft *ads;
	struct List locationList;
	struct List symbolList;
	struct List sdlList;
	struct Node *firstSdl;
	APTR currentSegment;
	APTR segments[16];
	struct Process *process;
	APTR oldTrapCode;
	struct BreakPoint *brokenBreakPoint;
	BOOL deleteBrokenBreakPoint;
	BOOL resumeToEnd;
	UWORD *transientBreakAddress;
	UWORD transientBreakRealOpCode;
	UWORD *immediateReturnAddress;
	APTR debuggerTask;
	ULONG brokenSignalMask;
	ULONG brokenPriority;
	ULONG debuggerMode;
	ULONG ourWindowWereFront;
	ULONG ourScreenWereFront;
	
	TEXT brokenFullPath[256];
	ULONG brokenBlockNum; // zero based
	TEXT contextFullPath[256]; // when broken, what variables show
	ULONG contextBlockNum; // zero based
	struct Node *contextNode; // NULL if first
	LONG normInfoWeight;
	LONG dbgInfoWeight;
	TEXT argString[256];
	struct Task *inputTask;
	ULONG onBreakShow;
};

#define NOTACTIVE 0
#define LOADING 1
#define RUNNING 2
#define BROKEN 3
#define RESUMING 4
#define STEPINTO 5
#define STEPOVER 6
#define STEPOUT  7
#define CLOSING 8

STRPTR LookupSourceLine(struct Debugger *dbgr,
	UWORD *brokenAddress,
	ULONG *blockNum, ULONG *spOffset);

void StartDebug(struct Debugger *dbgr, STRPTR filename);
void BreakDebug(struct Debugger *dbgr);
void ResumeDebug(struct Debugger *dbgr);
void StopDebug(struct Debugger *dbgr);
void StepIntoDebug(struct Debugger *dbgr);
void StepOverDebug(struct Debugger *dbgr);
void StepOutDebug(struct Debugger *dbgr);

void DebuggerOnDocHasChanged(struct Debugger *dbgr, struct CodecraftDoc *adsDoc, BOOL changed);

void DebuggerOnBroken(struct Debugger *dbgr);
BOOL InstallBreakPoint(struct BreakPoint *breakPoint);
void UninstallBreakPoint(struct BreakPoint *breakPoint);
void FreeBreakPoint(struct BreakPoint *breakPoint);
void UpdateDebuggerGUI(struct Debugger *dbgr);
