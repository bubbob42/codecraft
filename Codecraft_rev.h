#define VERSION		1
#define REVISION	13
#define DATE		"19.3.2023"
#define VERS		"Codecraft 1.13"
#define VSTRING		"Codecraft 1.13 (19.3.2023)\r\n"
#define VERSTAG		"\0$VER: Codecraft 1.13 (19.3.2023)"
