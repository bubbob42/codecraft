/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecraft is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include "debugger.h"

#include <exec/tasks.h>
#include <dos/dostags.h>
#include <dos/doshunks.h>
#include <libraries/iffparse.h>

#include <tools/textedit/extension.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/listbrowser.h>
#include <proto/clicktab.h>
#include <proto/alib.h>

#include "breakpoints.h"
#include "stackanalyser.h"
#include "wbreplytask.h"

#define CATCOMP_NUMBERS;
#include "strings.h"
const UBYTE *GetStr(LONG stringNum);

ULONG trapa(void);
UWORD *brokenAddress;
UWORD brokenSR;
struct DebuggerRegisters *brokenRegs;
ULONG opcodeStepping;
struct Debugger *g_debugger;
extern void loop(void);

ULONG parseSymbol(struct Debugger *dbgr, BPTR file)
{
	ULONG stringLen;
		
	FRead(file, &stringLen, sizeof(stringLen), 1);

	while (stringLen)
	{
		struct SymbolNode *symNode;
		symNode = AllocVec(1+sizeof(struct SymbolNode) + stringLen*sizeof(ULONG), MEMF_ANY);
	
		if (!symNode)
			return FALSE;
		
		symNode->node.ln_Name = (UBYTE *)&symNode[1];
		FRead(file, symNode->node.ln_Name, sizeof(ULONG), stringLen);
		FRead(file, &symNode->memPtr, sizeof(symNode->memPtr), 1);

		symNode->node.ln_Name[stringLen * sizeof(ULONG)] = 0;
		symNode->memPtr += 4 +(ULONG)dbgr->currentSegment;

		AddTail(&dbgr->symbolList, symNode);

		FRead(file, &stringLen, sizeof(stringLen), 1);
	}
	return TRUE;
}

ULONG parseDebug(struct Debugger *dbgr, BPTR file)
{
	ULONG totalLen;
	ULONG baseOffset;
	ULONG stringLen;
	ULONG i;
	ULONG typeID;
	
	FRead(file, &totalLen, sizeof(totalLen), 1);

	if (totalLen < 2)
		return TRUE;
		
	FRead(file, &baseOffset, sizeof(baseOffset), 1);
	FRead(file, &typeID, sizeof(typeID), 1);
	totalLen -= 2;
	
	if (typeID == MAKE_ID('L', 'I', 'N', 'E'))
	{
		struct SourceLineLocations *sll;
		BPTR lock;

		sll = AllocVec(sizeof(struct SourceLineLocations) + totalLen*8, MEMF_PUBLIC);
		if (!sll)
			return FALSE;
		
		sll->lines = (struct SLLLines *)(((UBYTE*)sll) + sizeof(struct SourceLineLocations));
		
		FRead(file, &stringLen, sizeof(ULONG), 1);
		totalLen--;
		FRead(file, sll->filename, sizeof(ULONG), stringLen);
		totalLen -= stringLen;
		sll->filename[stringLen*4] = 0; // null terminate
		sll->segment = dbgr->currentSegment;
		sll->segmentStartOffset = baseOffset;
		sll->segmentEndOffset = (*(((ULONG *)sll->segment) - 1)) - 8;

		i = 0;
		while (totalLen)
		{
			FRead(file, &sll->lines[i].blockNum, sizeof(ULONG), 1);
			sll->lines[i].spOffset = sll->lines[i].blockNum >> 24;
			sll->lines[i].blockNum &= 0xFFFFFF;
			sll->lines[i].blockNum--; // we are zero based
			totalLen--;
			FRead(file, &sll->lines[i].offset, sizeof(ULONG), 1);
			totalLen--;
			i++;
		}
		sll->size = i;
		
		// Let's save ourselves some trouble when stepping
		sll->fileExists = FALSE;
		if (*sll->filename && (lock = Lock(sll->filename, ACCESS_READ)))
		{
			sll->fileExists = TRUE;
			UnLock(lock);
		}
		
		// Finally put it in the list
		AddTail(&dbgr->locationList, (struct Node *)sll);
	}
	else if (typeID == MAKE_ID('S', 'R', 'C', '6'))
	{
		totalLen = ParseSRC6(dbgr, file, totalLen);
		if (dbgr->firstSdl == NULL)
			dbgr->firstSdl = dbgr->sdlList.lh_TailPred;
	}
		
	while (totalLen)
	{
		FRead(file, &stringLen, sizeof(stringLen), 1);
		totalLen--;
	}
	return TRUE;
}

ULONG parseHunkHeader(BPTR file)
{
	ULONG stringLen;
	ULONG tabSize;
	ULONG first;
	ULONG last;
	ULONG size;
	ULONG dummy;
	
	FRead(file, &stringLen, sizeof(stringLen), 1);

	while (stringLen)
	{
		while (stringLen--)
			FRead(file, &dummy, sizeof(dummy), 1);
			
		FRead(file, &stringLen, sizeof(stringLen), 1);
	}
	FRead(file, &tabSize, sizeof(tabSize), 1);
	FRead(file, &first, sizeof(first), 1);
	FRead(file, &last, sizeof(last), 1);
	
	while (tabSize--)
		FRead(file, &size, sizeof(size), 1);

	return TRUE;
}

ULONG parseCodeData(BPTR file)
{
	ULONG size;
	ULONG dummy;
	
	FRead(file, &size, sizeof(size), 1);
	
	while (size--)
		FRead(file, &dummy, sizeof(dummy), 1);
		
	return TRUE;
}

ULONG parseReloc32(BPTR file)
{
	ULONG size;
	ULONG dummy;
	
	FRead(file, &size, sizeof(size), 1);
	while (size)
	{
		FRead(file, &dummy, sizeof(dummy), 1);
	
		while (size--)
			FRead(file, &dummy, sizeof(dummy), 1);
			
		FRead(file, &size, sizeof(size), 1);
	}		
	return TRUE;
}

ULONG LoadDebugInfo(struct Debugger *dbgr, STRPTR filename)
{
	BPTR file;
	ULONG blockID;
	ULONG dummy;
	ULONG hunkNum = 0;
	ULONG ok = TRUE;
	struct SourceLineLocations *sll;
	ULONG endOffset = 0;
	APTR segment;
	
	file = Open(filename, MODE_OLDFILE);
	
	if (!file)
		return FALSE;
	
	dbgr->currentSegment = dbgr->segments[0];
	
	FRead(file, &blockID, sizeof(blockID), 1);

	ok = (blockID == 1011);
	
	while (ok)
	{
		blockID &= 0x3FFFFFFF;
		switch (blockID)
		{
			case 0:
				ok = FALSE;
				break;
				
			case HUNK_END:
				dbgr->currentSegment = dbgr->segments[++hunkNum];
				break;
				
			case HUNK_HEADER:
				ok = parseHunkHeader(file);
				break;
				
			case HUNK_CODE:
				ok = parseCodeData(file);
				dbgr->firstSdl = NULL;
				break;
			case HUNK_DATA:
				RegisterData(dbgr->firstSdl, hunkNum, ((UBYTE *)dbgr->currentSegment) + 4);
				ok = parseCodeData(file);
				break;
				
			case HUNK_BSS:
				RegisterBSS(dbgr->firstSdl, hunkNum, ((UBYTE *)dbgr->currentSegment) + 4);
				FRead(file, &dummy, sizeof(dummy), 1);
				break;

			case HUNK_RELOC32:
				ok = parseReloc32(file);
				break;
				
			case HUNK_SYMBOL:
				ok = parseSymbol(dbgr, file);
				break;
				
			case HUNK_DEBUG:
				ok = parseDebug(dbgr, file);
				break;
				
			default:
				kprintf("didn't understand blockID %ld\n", blockID);
				ok = FALSE;
				break;
		}
			
		if (! FRead(file, &blockID, sizeof(blockID), 1))
			break;
	}

	Close(file);
	
	// For now assume that sll appears with highest offsets first
	sll = (struct SourceLineLocations *)dbgr->locationList.lh_Head;
	segment = 0;
	
	while (sll->node.ln_Succ)
	{
		if (segment == sll->segment)
			sll->segmentEndOffset = endOffset;

		segment = sll->segment;
		endOffset = sll->segmentStartOffset - 2;
		sll = (struct SourceLineLocations *)sll->node.ln_Succ;
	}
	
	return ok;
}

void SplitIntoSegments(struct Debugger *dbgr, BPTR segment)
{
	ULONG i = 0;
	do
	{
		dbgr->segments[i++] = BADDR(segment);
		// get BPTR to next segment
		segment = *((BPTR *)BADDR(segment));
	} while (segment);
}

void installOpCode(UWORD *addr, UWORD opCode)
{
	if (addr)
		*addr = opCode;
	CacheClearU();
}

void SignalDebugger(struct Debugger *dbgr)
{
	Signal(dbgr->debuggerTask, dbgr->brokenSignalMask);
}

BOOL InstallBrakPointGrunt(struct Debugger *dbgr,
	struct BreakPoint *breakPoint,
	struct SourceLineLocations *sll,
	ULONG index)
{
	ULONG offset = sll->lines[index].offset + sll->segmentStartOffset;
	ULONG blockNum = sll->lines[index].blockNum;
	if (sll->lines[index].blockNum != breakPoint->blockNum)
	{
		struct BreakPoint *nextBp = (struct BreakPoint *)breakPoint->node.mln_Succ;
		while (nextBp->node.mln_Succ && nextBp->blockNum <= blockNum)
		{
			struct BreakPoint *deleteThis = nextBp;
			
			if (deleteThis->blockNum == blockNum && deleteThis->address)
				return FALSE;
			nextBp = (struct BreakPoint *)nextBp->node.mln_Succ;
			DeleteBreakPoint(deleteThis);
		}
		
		breakPoint->blockNum = blockNum;
	}

	if (breakPoint->disabled)
		return TRUE; // even if disabled we still found it
	
	// offset is relative to segment plus 4 bytes
	breakPoint->address = (UWORD *)(((UBYTE *)sll->segment) + offset + 4);
	
	CacheClearU();
	breakPoint->realOpCode = *breakPoint->address;
	installOpCode(breakPoint->address, 0x4afc);
	return TRUE;
}

void InstallAllBreakPoints(struct Debugger *dbgr)
{
	struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)dbgr->ads->documentList.mlh_Head;
	ULONG first = TRUE;
	
	while (adsDoc->node.mln_Succ)
	{
		struct BreakPoint *breakPoint = (struct BreakPoint *)adsDoc->breakPointList.mlh_Head;
		ULONG index = 0;
		struct SourceLineLocations *sll = (struct SourceLineLocations *)dbgr->locationList.lh_Head;

		adsDoc->changed = FALSE;
		
		while (sll->node.ln_Succ && Stricmp(adsDoc->fullPath, sll->filename) != 0)
			sll = (struct SourceLineLocations *)sll->node.ln_Succ;

		while (breakPoint->node.mln_Succ)
		{
			breakPoint->address = NULL;
			
			if (sll->node.ln_Succ)
			{
				while ((index < sll->size) && sll->lines[index].blockNum < breakPoint->blockNum)
					index++;

				// we know InstallBrakPointGrunt can't return FALSE here					
				if (index < sll->size)
					InstallBrakPointGrunt(dbgr, breakPoint, sll, index);
			}
			
			breakPoint = (struct BreakPoint *)breakPoint->node.mln_Succ;
		}
		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
		if (first && adsDoc->node.mln_Succ == NULL)
		{
			first = FALSE;
			adsDoc = (struct CodecraftDoc *)dbgr->ads->breakpointDocList.mlh_Head;
		}
	}
}

void UnInstallAllBreakPointsInDoc(struct Debugger *dbgr, struct CodecraftDoc *adsDoc)
{
	struct BreakPoint *breakPoint = (struct BreakPoint *)adsDoc->breakPointList.mlh_Head;

	while (breakPoint->node.mln_Succ)
	{
		if (breakPoint->address && breakPoint != dbgr->brokenBreakPoint)
		{
			installOpCode(breakPoint->address, breakPoint->realOpCode);
			breakPoint->address = NULL;
		}
					
		breakPoint = (struct BreakPoint *)breakPoint->node.mln_Succ;
	}
}

void UnInstallAllBreakPoints(struct Debugger *dbgr)
{
	struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)dbgr->ads->documentList.mlh_Head;
	ULONG first = TRUE;
	
	while (adsDoc->node.mln_Succ)
	{
		UnInstallAllBreakPointsInDoc(dbgr, adsDoc);

		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
		if (first && adsDoc->node.mln_Succ == NULL)
		{
			// done with open documents, switch to closed doc list
			first = FALSE;
			adsDoc = (struct CodecraftDoc *)dbgr->ads->breakpointDocList.mlh_Head;
		}
	}
}

// returns false if breakpoint is invalid and should be deleted
BOOL InstallBreakPoint(struct BreakPoint *breakPoint)
{
	struct CodecraftDoc *adsDoc = breakPoint->adsDoc;
	struct Debugger *dbgr = adsDoc->ads->debugger;
	struct SourceLineLocations *sll;
	ULONG index = 0;

	if (dbgr->resumeToEnd)
		return TRUE;

	if (breakPoint->address)
		return TRUE;

	if (dbgr->debuggerMode == NOTACTIVE)
		return TRUE;
		
	if (adsDoc->changed)
		return TRUE;

	sll = (struct SourceLineLocations *)dbgr->locationList.lh_Head;

	while (sll->node.ln_Succ && Stricmp(adsDoc->fullPath, sll->filename) != 0)
		sll = (struct SourceLineLocations *)sll->node.ln_Succ;

	if (! sll->node.ln_Succ)
		return TRUE;
	
	while ((index < sll->size) && sll->lines[index].blockNum < breakPoint->blockNum)
		index++;
		
	if (index < sll->size)
		return InstallBrakPointGrunt(dbgr, breakPoint, sll, index);
}

void UninstallBreakPoint(struct BreakPoint *breakPoint)
{
	if (! breakPoint->address)
		return;
		
	CacheClearU();
	installOpCode(breakPoint->address, breakPoint->realOpCode);
	breakPoint->address = NULL;
}

void FreeBreakPoint(struct BreakPoint *breakPoint)
{
	struct CodecraftDoc *adsDoc = breakPoint->adsDoc;
	struct Debugger *dbgr = adsDoc->ads->debugger;
	
	if (dbgr->brokenBreakPoint == breakPoint)
		dbgr->deleteBrokenBreakPoint = TRUE;
	else
		FreeVec(breakPoint);
}

void DebuggerOnDocHasChanged(struct Debugger *dbgr, struct CodecraftDoc *adsDoc, BOOL changed)
{
	if (dbgr->debuggerMode == NOTACTIVE)
		return;

	if (dbgr->ads->buildInProgress)
		return;

	if (adsDoc->changed == changed)
		return; // already handle
	
	adsDoc->changed = changed;
	
	CacheClearU();
	UnInstallAllBreakPointsInDoc(dbgr, adsDoc);
}

// on return blockNum is zero based
STRPTR LookupSourceLine(struct Debugger *dbgr,
	UWORD *brokenAddress,
	ULONG *blockNum, ULONG*spOffset)
{
	struct SourceLineLocations *sll = (struct SourceLineLocations *)dbgr->locationList.lh_Head;

	while (sll->node.ln_Succ)
	{
		ULONG baseAddress = ((ULONG)sll->segment) + 4;
		ULONG index = 0;
		
		if (sll->fileExists && ((ULONG)brokenAddress) > baseAddress)
		{
			ULONG offset = ((ULONG)brokenAddress) - baseAddress;
			ULONG prevBlock = 0;
			ULONG prevSpOffset = 0;
						
			if ((offset >= sll->segmentStartOffset) && (offset <= sll->segmentEndOffset))
			{
				offset -= sll->segmentStartOffset; // make it relative
				while ((index < sll->size))
				{	
					if (sll->lines[index].offset > offset)
					{
						*blockNum = prevBlock;
						if (spOffset)
							*spOffset = prevSpOffset;
						return sll->filename;
					}
					
					prevBlock = sll->lines[index].blockNum;
					prevSpOffset = sll->lines[index].spOffset;
					index++;
				}
				if (sll->lines[index-1].offset + 0xFF < offset)
					return NULL; // some sanity check
				// We want the last line
				*blockNum = prevBlock;
				if (spOffset)
					*spOffset = prevSpOffset;
				return sll->filename;
			}
 		}
		
		sll = (struct SourceLineLocations *)sll->node.ln_Succ;
	}

	return NULL;
}

struct BreakPoint *LookupBreakPoint(struct Debugger *dbgr, UWORD *address)
{
	struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)dbgr->ads->documentList.mlh_Head;
	ULONG first = TRUE;

	while (adsDoc->node.mln_Succ)
	{
		struct BreakPoint *breakPoint = (struct BreakPoint *)adsDoc->breakPointList.mlh_Head;

		while (breakPoint->node.mln_Succ)
		{
			if (breakPoint->address == address)
				return breakPoint;
						
			breakPoint = (struct BreakPoint *)breakPoint->node.mln_Succ;
		}
		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
		if (first && adsDoc->node.mln_Succ == NULL)
		{
			first = FALSE;
			adsDoc = (struct CodecraftDoc *)dbgr->ads->breakpointDocList.mlh_Head;
		}
	}
				
	return NULL;
}

void UpdateDebuggerGUI(struct Debugger *dbgr)
{
	struct Codecraft *ads = dbgr->ads;
	BOOL gr1,gr2,gr3;
	UBYTE *sp;
	
	switch (dbgr->debuggerMode)
	{
		case NOTACTIVE:
			clearCallStackBrowser(ads);
			clearVariableBrowser(ads);
		    gr1 = TRUE;
		    gr2 = gr3 = FALSE;
			ScreenToFront(ads->window->WScreen);
			WindowToFront(ads->window);
		    ActivateWindow(ads->window);
			dbgr->dbgInfoWeight = ads->tei->weightOfEdgeGadget(TEEdge_Bottom, ads->utilityClickTab);
			ads->tei->resizeEdgeGadget(TEEdge_Bottom, ads->utilityClickTab, dbgr->normInfoWeight);
			break;
			
		case RUNNING:
			SetGadgetAttrs(	(struct Gadget *) ads->variableBrowser, ads->window, NULL,
				LISTBROWSER_Labels, (ULONG) NULL,
				TAG_DONE
				);
			gr1 = FALSE;
			gr2 = TRUE;
			gr3 = FALSE;	
			break;
			
		case BROKEN:
			sp = UpdateCallStackBrowser(ads, brokenAddress);
			UpdateVariableBrowser(dbgr, dbgr->brokenFullPath, dbgr->brokenBlockNum, sp);
			SetGadgetAttrs(	(struct Gadget *) ads->variableBrowser, ads->window, NULL,
				LISTBROWSER_Labels, &ads->variableBrowserList,
				TAG_DONE
				);
			gr1 = FALSE;
			gr2 = FALSE;
			gr3 = TRUE;
			dbgr->ourScreenWereFront = IntuitionBase->FirstScreen == ads->window->WScreen;
			dbgr->ourWindowWereFront = ads->window->WScreen->FirstWindow == ads->window;
			ScreenToFront(ads->window->WScreen);
			WindowToFront(ads->window);
		    ActivateWindow(ads->window);
			break;
			
		case RESUMING:
			if (!dbgr->ourScreenWereFront)
				ScreenToBack(ads->window->WScreen);
			if (!dbgr->ourWindowWereFront)
				WindowToBack(ads->window);
			// intentional fallthrough
		default:
			gr1 = FALSE;
			gr2 = FALSE;
			gr3 = FALSE;
			break;				
	}

	if (ads->buildInProgress)
		gr1 = gr2 = gr2 = FALSE;

	UpdateMenuStates(ads, gr1, gr2, gr3);
			
	// Make sure the new state of breakpoints are rendered
    ads->tei->redrawLeftBar(ads->currentAdsDocument->document);
}

LONG __ASM__ onProgramDone(__REG__(d0, LONG retval), __REG__(d1, struct Debugger *dbgr))
{
	dbgr->debuggerMode = CLOSING;
	SignalDebugger(dbgr);
	opcodeStepping = FALSE;

	return retval;
}

void SetTransientBreak(struct Debugger *dbgr, UWORD *breakPC)
{
	// first undo if we have one already
	if (dbgr->transientBreakAddress)
	{
		installOpCode(dbgr->transientBreakAddress, dbgr->transientBreakRealOpCode); // illegal instruction
	}
	
	dbgr->transientBreakAddress = breakPC;
	
	// and then potentially install it
	if (breakPC)
	{
		dbgr->transientBreakRealOpCode = *breakPC;
		installOpCode(breakPC, 0x4afc); // illegal instruction
	}
}

void StartDebug(struct Debugger *dbgr, STRPTR filename)
{
    BPTR seglist;
    struct Codecraft *ads = dbgr->ads;
    
    dbgr->debuggerMode = LOADING;
    dbgr->resumeToEnd = FALSE;
	dbgr->brokenBreakPoint = NULL;
	dbgr->deleteBrokenBreakPoint = FALSE;
	UpdateDebuggerGUI(dbgr);
	setWindowTitles(dbgr->ads);
	    
    seglist = NewLoadSeg(filename, TAG_DONE);

	SplitIntoSegments(dbgr, seglist);

	NewList(&dbgr->locationList);
	NewList(&dbgr->symbolList);
	NewList(&dbgr->sdlList);
    
	if (! LoadDebugInfo(dbgr, filename))
		kprintf("loading debug info failed\n");
	
	if (dbgr->symbolList.lh_Head->ln_Succ == NULL)
	{
		if (! askUser(ads->window, MSG_DEBUGGER_RUNORNOT_GAD, MSG_DEBUGGER_NODBG, NULL))
		{
			dbgr->debuggerMode = CLOSING;
			DebuggerOnBroken(dbgr);
			return;
		}
	}
	
	InstallAllBreakPoints(dbgr);
	UpdateBreakPointBrowser(ads);
	
	dbgr->debuggerMode = RUNNING;
	dbgr->normInfoWeight = ads->tei->weightOfEdgeGadget(TEEdge_Bottom, ads->utilityClickTab);
	ads->tei->resizeEdgeGadget(TEEdge_Bottom, ads->utilityClickTab, dbgr->dbgInfoWeight);
	UpdateDebuggerGUI(dbgr);
	dbgr->transientBreakAddress = NULL;

	Forbid();

	if (ads->project.flags & CCPROJ_FLAG_WB)
		dbgr->process = runAsIfFromWb(ads, seglist,
			NP_ExitCode, onProgramDone,
			NP_ExitData, dbgr,
			TAG_DONE);
	else
		dbgr->process = runAsIfFromCLI(ads, seglist,
			NP_ExitCode, onProgramDone,
			NP_ExitData, dbgr,
			TAG_DONE);
			
	if (dbgr->process)
	{
		dbgr->oldTrapCode = dbgr->process->pr_Task.tc_TrapCode;
	
	    // Point task to our assembler trap handler code.
	    dbgr->process->pr_Task.tc_TrapCode = (APTR)trapa;
	}
	else
	{
		dbgr->debuggerMode = CLOSING;
		DebuggerOnBroken(dbgr);
	}
	
	Permit();
}

void resumeProgram(struct Debugger *dbgr)
{
	if (dbgr->brokenBreakPoint)
	{
		installOpCode(dbgr->brokenBreakPoint->address, dbgr->brokenBreakPoint->realOpCode);

		if (dbgr->deleteBrokenBreakPoint)
		{
			FreeVec(dbgr->brokenBreakPoint);
			dbgr->brokenBreakPoint = NULL;
			dbgr->deleteBrokenBreakPoint = FALSE;
		}
	}
	installOpCode((UWORD *)loop, 0x4afc); // illegal instruction
}

void ResumeFromBroken(struct Debugger *dbgr)
{
	resumeProgram(dbgr);
	SetTaskPri(&dbgr->process->pr_Task, dbgr->brokenPriority);
}

void BreakDebug(struct Debugger *dbgr)
{
	UBYTE *sp;
	UWORD *pc;

	dbgr->brokenPriority = dbgr->process->pr_Task.tc_Node.ln_Pri;
	SetTaskPri(&dbgr->process->pr_Task, -127);
	
	sp = dbgr->process->pr_Task.tc_SPReg;
	pc = (UWORD *)*((ULONG *)(sp+4));
	
	SetTransientBreak(dbgr, pc);
	
	SetTaskPri(&dbgr->process->pr_Task, dbgr->brokenPriority);
}

void ResumeDebug(struct Debugger *dbgr)
{
	opcodeStepping = TRUE;
	dbgr->debuggerMode = RESUMING;
	UpdateDebuggerGUI(dbgr);

	ResumeFromBroken(dbgr);
}

void StopDebug(struct Debugger *dbgr)
{
	CacheClearU();
	UnInstallAllBreakPoints(dbgr); // uninstall existing
	dbgr->resumeToEnd = TRUE; // don't install any new ones
	ResumeDebug(dbgr);
}

void StepIntoDebug(struct Debugger *dbgr)
{
	opcodeStepping = TRUE;
	dbgr->debuggerMode = STEPINTO;
	UpdateDebuggerGUI(dbgr);

	ResumeFromBroken(dbgr);
}

void StepOverSubroutine(struct Debugger *dbgr)
{
	UWORD opcode = *brokenAddress;
	
	SetTransientBreak(dbgr, NULL);
	
	if (dbgr->brokenBreakPoint && brokenAddress == dbgr->brokenBreakPoint->address)
		opcode = dbgr->brokenBreakPoint->realOpCode;
		
	if (((0xFFF8 & opcode) == 0x4E90) // JSR reg
		|| (((0xFF00 & opcode) == 0x6100)
		 && ((0xFF & opcode) != 0x00)
		 && ((0xFF & opcode) != 0xFF))) //BSR UBYTE
	{
		SetTransientBreak(dbgr, brokenAddress + 1);
	}
	else
	{
		if (((0xFFF8 & opcode) == 0x4EA8) // JSR UWORD(reg)
			|| ((0xFFF8 & opcode) == 0x4EE0) // JSR UBYTE(reg,reg)
			|| (opcode == 0x4EB8) // JSR UWORD
			|| (opcode == 0x4EBA) // JSR UWORD(pc)
			|| (opcode == 0x4EBB) // JSR UBYTE(pc,reg)
			|| (((0xFF00 & opcode) == 0x6100)
			 && ((0xFF & opcode) != 0xFF))) // BSR UWORD
			SetTransientBreak(dbgr, brokenAddress + 2);
		else
			if ((opcode == 0x4EB9) // JSR ULONG
				|| (opcode == 0x61FF)) // BSR ULONG
				SetTransientBreak(dbgr, brokenAddress + 3);
	}
		
	if (dbgr->transientBreakAddress)
		opcodeStepping = FALSE;
	else
		opcodeStepping = TRUE;
}

void StepOverDebug(struct Debugger *dbgr)
{
	StepOverSubroutine(dbgr);
	dbgr->debuggerMode = STEPOVER;
	UpdateDebuggerGUI(dbgr);

	ResumeFromBroken(dbgr);
}

void StepOutSubroutine(struct Debugger *dbgr)
{
	SetTransientBreak(dbgr, dbgr->immediateReturnAddress);

	opcodeStepping = FALSE;
}
void StepOutDebug(struct Debugger *dbgr)
{
	dbgr->debuggerMode = STEPOUT;
	StepOutSubroutine(dbgr);
	UpdateDebuggerGUI(dbgr);

	ResumeFromBroken(dbgr);
}

void BreakHandler(void)
{
	struct Debugger *dbgr = g_debugger;
	
	if (dbgr->brokenBreakPoint)
	{
		// we are now past the breakpoint and can reinstall
		// the illegal opcode
		if (dbgr->brokenBreakPoint->address)
		{
			if(!dbgr->resumeToEnd && ! dbgr->brokenBreakPoint->adsDoc->changed)
				installOpCode(dbgr->brokenBreakPoint->address, 0x4afc); // illegal instruction
			else
				dbgr->brokenBreakPoint->address = NULL;
		}
		
		dbgr->brokenBreakPoint = NULL;
	}

	if (dbgr->debuggerMode == RESUMING)
	{
		// we are trying to resume
		opcodeStepping = FALSE;
		dbgr->debuggerMode = RUNNING;
		SignalDebugger(dbgr);
		resumeProgram(dbgr);
	}
	else
	{
		ULONG blockNum;
		STRPTR fullPath = NULL;
		
		dbgr->brokenBreakPoint = LookupBreakPoint(dbgr, brokenAddress);
		if (dbgr->brokenBreakPoint)
		{
			blockNum = dbgr->brokenBreakPoint->blockNum;
			fullPath = dbgr->brokenBreakPoint->adsDoc->fullPath;
		} else
		{
			fullPath = LookupSourceLine(dbgr, brokenAddress, &blockNum, NULL);
		}

		if (dbgr->transientBreakAddress == brokenAddress)
		{
			installOpCode(dbgr->transientBreakAddress, dbgr->transientBreakRealOpCode);
			dbgr->transientBreakAddress = 0;
		}
		
		// Check if we are not allowed to break due to:
		//    - Forbid()
		//    - inside input.device task
		if (SysBase->TDNestCnt != -1 || dbgr->inputTask == FindTask(NULL))
		{
			if (dbgr->brokenBreakPoint)
			{
				opcodeStepping = TRUE;
				dbgr->debuggerMode = RESUMING;
			}

			resumeProgram(dbgr);
			return;
		}
		
		if (fullPath)
		{
			switch (dbgr->debuggerMode)
			{
				case STEPOUT:
					if ((Stricmp(dbgr->brokenFullPath, fullPath) != 0)
								|| dbgr->brokenBlockNum != blockNum)
						dbgr->debuggerMode = BROKEN;
					else
						StepOutSubroutine(dbgr);
					break;
					
				case STEPOVER:
					if (dbgr->brokenBlockNum == blockNum
						 && (Stricmp(dbgr->brokenFullPath, fullPath) == 0))
					{
						StepOverSubroutine(dbgr);
						break;
					}
					
					//intentional fallthrough to default
					if (FALSE)
				
				case STEPINTO:
					if (dbgr->brokenBlockNum == blockNum
						 && (Stricmp(dbgr->brokenFullPath, fullPath) == 0))
						break;
					//intentional fallthrough
				
				default:
					dbgr->debuggerMode = BROKEN;
					if (dbgr->onBreakShow)
					{
						SetGadgetAttrs(	(struct Gadget *) dbgr->ads->utilityClickTab, dbgr->ads->window, NULL,
							CLICKTAB_Current, 1 + dbgr->onBreakShow,
							TAG_DONE
							);
					}
					break;
			}
		}
		// if (!bp && trap != opcodestepping) we hit a program bug
		//  but it is unknown at which sourcetay RUNNING
		
		if (dbgr->debuggerMode == BROKEN)
		{
			dbgr->brokenBlockNum = blockNum;
			dbgr->contextBlockNum = blockNum;

			*dbgr->brokenFullPath = 0;
			*dbgr->contextFullPath = 0;
			if (fullPath)
			{
				Strncpy(dbgr->brokenFullPath, fullPath, sizeof(dbgr->contextFullPath));
				Strncpy(dbgr->contextFullPath, fullPath, sizeof(dbgr->contextFullPath));
			}
			dbgr->contextNode = NULL;
			SignalDebugger(dbgr);
		}
		else
			resumeProgram(dbgr);
	}
}

void DebuggerOnBroken(struct Debugger *dbgr)
{
	struct Codecraft *ads = dbgr->ads;
	
	if (dbgr->debuggerMode == CLOSING)
	{
		struct SourceLineLocations *sll;
		struct Node *symNode;
		
		opcodeStepping = FALSE;
		dbgr->debuggerMode = NOTACTIVE;
		UnInstallAllBreakPoints(dbgr);
		UpdateDebuggerGUI(dbgr);
		setWindowTitles(ads);
		while (sll = (struct SourceLineLocations *)RemTail(&dbgr->locationList))
			FreeVec(sll);
		while (symNode = RemTail(&dbgr->symbolList))
			FreeVec(symNode);
		FreeSRC6(dbgr);
	}
	else if (dbgr->debuggerMode == RUNNING)
		UpdateDebuggerGUI(dbgr);
	else if (dbgr->debuggerMode == BROKEN)
	{
		dbgr->brokenPriority = dbgr->process->pr_Task.tc_Node.ln_Pri;
		SetTaskPri(&dbgr->process->pr_Task, -128);
		
		revealLocation(ads, dbgr->brokenFullPath, dbgr->brokenBlockNum);
		UpdateDebuggerGUI(dbgr);
	}
}

struct Debugger *CreateDebugger(struct Codecraft *ads)
{
	struct Debugger *dbgr = AllocMem(sizeof(struct Debugger), MEMF_PUBLIC|MEMF_CLEAR);
	
	g_debugger = dbgr;
	if (dbgr)
	{
		struct MsgPort *mp;
		dbgr->ads = ads;
		dbgr->debuggerMode = NOTACTIVE;
		dbgr->debuggerTask = FindTask(0);
		dbgr->brokenSignalMask = 1<<AllocSignal(-1);
		ads->expandVariableMode = FALSE;

		dbgr->inputTask == FindTask("input.device");

		dbgr->normInfoWeight = 10;
		dbgr->dbgInfoWeight = 50;
		
		Forbid();
		mp = FindPort(WBREPLYPORTNAME);
		if (! mp)
		{
			BPTR seglist;

		    seglist = NewLoadSeg("CodecraftWbHelper", TAG_DONE);
		    if (seglist)
		    {
			    CreateNewProcTags(
					NP_Seglist, seglist,
					NP_FreeSeglist, TRUE,
					NP_StackSize, 55512,
					NP_Name, "CodecraftWbHelper",
					NP_Output,0,
					NP_CloseOutput,FALSE,
					TAG_DONE);
		    }			
		}
		Permit();
	}
	return dbgr;
}

