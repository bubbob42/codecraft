/* This file is part of Codecraft, an IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */
#ifndef BREAKPOINTS_H
#define BREAKPOINTS_H

#include <math.h>

#include <intuition/classusr.h>
#include <gadgets/texteditor.h>

struct BreakPoint
{
    struct MinNode node;
    ULONG blockNum;
	BOOL disabled:1;
	BOOL autocontinue:1;
	UWORD realOpCode;
	UWORD *address;
	struct CodecraftDoc *adsDoc;
	struct Node *browserNode;
};

struct ChangeListenerCodecraft
{
    struct ChangeListener listener;
    struct CodecraftDoc *adsDoc;
};

struct BreakPoint *BreakPointsFindLessOrEqual(struct CodecraftDoc *adsDoc, struct BreakPoint *currentBreakPoint, ULONG blockNum);
void BreakPointsOnCharsDeleted(struct ChangeListener *listener, ULONG startBlockNum, ULONG startPosInBlock,
                             ULONG endBlockNum, ULONG endPosInBlock);
void BreakPointsOnCharsInserted(struct ChangeListener *listener, ULONG startBlockNum, ULONG startPosInBlock,
                    ULONG endBlockNum, ULONG endPosInBlock);
void BreakPointsToggleAtBlockNum(struct CodecraftDoc *adsDoc, ULONG blockNum);
void revealBreakPoint(struct BreakPoint *breakpoint);
void setDisabledBreakPoint(struct Codecraft *ads, struct Node *browserNode, BOOL disabled);
void deleteAllBreakPoints(struct Codecraft *ads);
void setDisableAllBreakPoints(struct Codecraft *ads, BOOL disabled);
void DeleteBreakPoint(struct BreakPoint *breakPoint);
void UpdateBreakPointBrowser(struct Codecraft *ads);
void resetBreakPointImages(struct Codecraft *ads);

#endif // BREAKPOINTS_H