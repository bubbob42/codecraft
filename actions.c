/* This file is part of Codecraft, and IDE running under AmigaOS 3.2.1 and later
 *
 * Copyright 2022 Camilla Boemann
 *
 * Codecraft is free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2 as published by the Free Software Foundation.
 *
 * Codecrafr is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with Foobar. If not,
 * see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>

#include <intuition/classusr.h>
#include <libraries/gadtools.h>

#include <proto/exec.h>
#include <proto/dos.h>

#define __CLIB_PRAGMA_LIBCALL
#include <clib/alib_protos.h>

#include <proto/intuition.h>
#include <proto/utility.h>
#include <proto/asl.h>
#include <proto/locale.h>
#include <proto/gadtools.h>
#include <proto/dos.h>

#include <tools/textedit/extension.h>
#include <tools/textedit/plugin.h>
#include "breakpoints.h"
#include "codecraft.h"
#include "debugger.h"

#define CATCOMP_NUMBERS;
#include "strings.h"
const UBYTE *GetStr(LONG stringNum);

extern struct Library *UtilityBase;
extern struct Locale *Locale;
extern struct Catalog *Catalog;
extern struct Library *AslBase;
extern struct Library *GadToolsBase;

static BOOL actionNewProjectTree(void);
static BOOL actionOpenProjectTree(void);
static BOOL actionRecent0(void);
static BOOL actionRecent1(void);
static BOOL actionRecent2(void);
static BOOL actionRecent3(void);
static BOOL actionRecent4(void);
static BOOL actionRecent5(void);
static BOOL actionRecent6(void);
static BOOL actionSaveAll(void);
static BOOL actionParameters(void);
static BOOL actionToggleBreakpoint(void);
static BOOL actionBuildProjectTree(void);
static BOOL actionCleanProjectTree(void);
static BOOL actionRebuildProjectTree(void);
static BOOL	actionPrevMessage(void);
static BOOL	actionNextMessage(void);
static BOOL actionRunDebug(void);
static BOOL actionRunNoDebug(void);
static BOOL actionBreakDebug(void);
static BOOL actionResumeDebug(void);
static BOOL actionStopDebug(void);
static BOOL actionStepIntoDebug(void);
static BOOL actionStepOverDebug(void);
static BOOL actionStepOutDebug(void);
static BOOL actionDeleteAllBookmarks(void);
static BOOL actionEnableAllBookmarks(void);
static BOOL actionDisableAllBookmarks(void);
static BOOL actionOnBrkDont(void);
static BOOL actionOnBrkVar(void);
static BOOL actionOnBrkCall(void);


#define IEQUALIFIER_SHIFT	(IEQUALIFIER_LSHIFT|IEQUALIFIER_RSHIFT)

static struct NewMenu newmenuSnip[] =
{
	{ NM_TITLE, (STRPTR)MSG_MENU_PROJECTTREE, 0, 0, 0, 0},
	{ NM_ITEM, (STRPTR)MSG_MENU_PT_NEW, 0, 0, 0, actionNewProjectTree},
	{ NM_ITEM, (STRPTR)MSG_MENU_PT_OPEN, "O", MIF_SHIFTCOMMSEQ, 0, actionOpenProjectTree},
	{ NM_ITEM, NM_BARLABEL, 0, 0, 0, 0},
	{ NM_ITEM, (STRPTR)MSG_MENU_PT_SAVEALL, 0, ITEMENABLED, 0, actionSaveAll},
	{ NM_ITEM, NM_BARLABEL, 0, 0, 0, 0},
	{ NM_ITEM, (STRPTR)MSG_MENU_PT_PARAMS, 0, ITEMENABLED, 0, actionParameters},
	{ NM_ITEM, NM_BARLABEL, 0, 0, 0, 0},
	{ NM_ITEM, (STRPTR)MSG_MENU_PT_RECENT, 0, 0, 0, 0},
	{ NM_SUB,  NULL, 0, ITEMENABLED, 0, 0},

	{ NM_TITLE, (STRPTR)MSG_MENU_BUILD, 0, 0, 0, 0},
	{ NM_ITEM, (STRPTR)MSG_MENU_BLD_BUILD, "F6", ITEMENABLED|COMMSEQ, 0, actionBuildProjectTree},
	{ NM_ITEM, (STRPTR)MSG_MENU_BLD_CLEAN, 0, ITEMENABLED, 0, actionCleanProjectTree},
	{ NM_ITEM, (STRPTR)MSG_MENU_BLD_REBUILD, "Ctrl F6", ITEMENABLED|COMMSEQ, 0, actionRebuildProjectTree},
    { NM_ITEM, NM_BARLABEL, 0, 0, 0, 0},
    { NM_ITEM, (STRPTR)MSG_MENU_BLD_PREVMSG, "F8", MIF_SHIFTCOMMSEQ|COMMSEQ, 0, actionPrevMessage},
    { NM_ITEM, (STRPTR)MSG_MENU_BLD_NEXTMSG, "F8", COMMSEQ, 0, actionNextMessage},

	{ NM_TITLE, (STRPTR)MSG_MENU_DEBUG,       0 , 0, 0, 0,},
	{ NM_ITEM, (STRPTR)MSG_MENU_DBG_RUNDBG,       "F5", ITEMENABLED|COMMSEQ, 0, actionRunDebug},
	{ NM_ITEM, (STRPTR)MSG_MENU_DBG_RUNWITHOUT,       "Ctrl F5", ITEMENABLED|COMMSEQ, 0, actionRunNoDebug},
	{ NM_ITEM, (STRPTR)MSG_MENU_DBG_BREAK,       0, ITEMENABLED, 0, actionBreakDebug},
    { NM_ITEM, NM_BARLABEL,  0 , 0, 0, 0,},
	{ NM_ITEM, (STRPTR)MSG_MENU_DBG_RESUME,       "F5", ITEMENABLED|COMMSEQ, 0, actionResumeDebug},
	{ NM_ITEM, (STRPTR)MSG_MENU_DBG_STOPDBG,       "F5", ITEMENABLED|MIF_SHIFTCOMMSEQ|COMMSEQ, 0, actionStopDebug},
    { NM_ITEM, (STRPTR)MSG_MENU_DBG_STEPINTO,       "Ctrl F10", ITEMENABLED|COMMSEQ, 0, actionStepIntoDebug},
    { NM_ITEM, (STRPTR)MSG_MENU_DBG_STEPOVER,       "F10", ITEMENABLED|COMMSEQ, 0, actionStepOverDebug},
    { NM_ITEM, (STRPTR)MSG_MENU_DBG_STEPOUT,       "F10", ITEMENABLED|MIF_SHIFTCOMMSEQ|COMMSEQ, 0, actionStepOutDebug},
    { NM_ITEM, NM_BARLABEL,  0 , 0, 0, 0,},
    { NM_ITEM, (STRPTR)MSG_MENU_DBG_TGLBRK,       "F9", COMMSEQ, 0, actionToggleBreakpoint},
    { NM_ITEM, (STRPTR)MSG_MENU_DBG_DELALLBRK,       0, ITEMENABLED, 0, actionDeleteAllBookmarks},
    { NM_ITEM, (STRPTR)MSG_MENU_DBG_ENALLBRK,       0, ITEMENABLED, 0, actionEnableAllBookmarks},
    { NM_ITEM, (STRPTR)MSG_MENU_DBG_DISALLBRK,       0, ITEMENABLED, 0, actionDisableAllBookmarks},

    { NM_END, NULL, 0, 0, 0, 0,},
};

static struct NewMenu recentmenuSnip[] =
{
	{ NM_SUB,  NULL, 0, 0, 0, actionRecent0},
	{ NM_SUB,  NULL, 0, 0, 0, actionRecent1},
	{ NM_SUB,  NULL, 0, 0, 0, actionRecent2},
	{ NM_SUB,  NULL, 0, 0, 0, actionRecent3},
	{ NM_SUB,  NULL, 0, 0, 0, actionRecent4},
	{ NM_SUB,  NULL, 0, 0, 0, actionRecent5},
	{ NM_SUB,  NULL, 0, 0, 0, actionRecent6},
	{ NM_END, NULL, 0, 0, 0, 0,},
};

static struct NewMenu settingmenuSnip[] =
{
    { NM_ITEM, NM_BARLABEL,  0 , 0, 0, 0,},
    { NM_ITEM, (STRPTR)MSG_MENU_SETTING_ONBRK,     0, 0, 0, 0},
	{ NM_SUB,  (STRPTR)MSG_MENU_SETTING_DONT, 0, CHECKIT|CHECKED, ~1, actionOnBrkDont},
	{ NM_SUB,  (STRPTR)MSG_MENU_SETTING_VAR, 0, CHECKIT , ~2	, actionOnBrkVar},
	{ NM_SUB,  (STRPTR)MSG_MENU_SETTING_CALL, 0, CHECKIT , ~4, actionOnBrkCall},
	{ NM_END, NULL, 0, 0, 0, 0,},
};


extern struct Codecraft myCodecraft;


void InitMenus(struct Codecraft *ads)
{
	// main menu
	struct NewMenu *mptr = newmenuSnip;

	/* install strings rather than string id in the newmenu array */
	while (mptr->nm_Type != NM_END)
	{
		if (mptr->nm_Label != NM_BARLABEL)
			mptr->nm_Label = (STRPTR)GetStr((LONG)mptr->nm_Label);
		mptr++;
	}

    ads->menu = CreateMenusA(newmenuSnip, NULL);
    ads->tei->insertMenu(1, ads->menu);
	ads->doRecentMenu = -1;

	// settings menu
	mptr = settingmenuSnip;

	/* install strings rather than string id in the newmenu array */
	while (mptr->nm_Type != NM_END)
	{
		if (mptr->nm_Label != NM_BARLABEL)
			mptr->nm_Label = (STRPTR)GetStr((LONG)mptr->nm_Label);
		mptr++;
	}

    ads->settingsmenu = CreateMenusA(settingmenuSnip, NULL);
    ads->tei->insertMenu(2, ads->settingsmenu);
}

ULONG GetBlockNum(struct Codecraft *ads)
{
    ULONG blockNum = 0;
	STRPTR res = ads->tei->executeRexxCommand(ads->currentAdsDocument->document, "GETCURSOR LINE");

    if (res > (STRPTR)TRUE)
    {
        blockNum = atoi(res);
        FreeVec(res);
    }
    return blockNum;
}


void updateRecentMenu(struct Codecraft *ads)
{
	UWORD i = 0;
	BPTR file;
	struct Menu *windowMenu;

	if (! ads->window)
		return;

	windowMenu = ads->window->MenuStrip;

	ClearMenuStrip(ads->window);

	if (ads->recentMenuItems)
		FreeMenus((struct Menu *)ads->recentMenuItems);

	if (ads->projectDirLock)
	{
		SNPrintf(ads->currentRecentBuffer, sizeof(ads->currentRecentBuffer), "%s - ", ads->project.name);
		ads->currentRecentBufferOffset = strlen(ads->currentRecentBuffer);
		NameFromLock(ads->projectDirLock, ads->currentRecentBuffer + ads->currentRecentBufferOffset, 128);
		strcat(ads->currentRecentBuffer, "/");
		strcat(ads->currentRecentBuffer, ads->project.name);
		strcat(ads->currentRecentBuffer, ".projecttree");
		recentmenuSnip[i].nm_Type = NM_SUB;
		recentmenuSnip[i].nm_Label = ads->currentRecentBuffer;
		recentmenuSnip[i].nm_Flags &= ~ITEMENABLED;
		i++;
	}

	file = Open("ENVARC:Codecraft/recent", MODE_OLDFILE);

	if (file)
	{
		STRPTR s = ads->recentBuffer;
		LONG len;
		Seek(file, 0, OFFSET_END);
		len = Seek(file, 0, OFFSET_BEGINNING);
		Read(file, ads->recentBuffer, len);

		while (*s && i < 7)
		{
			STRPTR end = ++s;

			recentmenuSnip[i].nm_Type = NM_SUB;
			recentmenuSnip[i].nm_Label = s;

			while (*end && *end != '\n')
				end++;
			if (*end)
				*end++ = 0;

			// this little hack to mack sure current project is not repeated
			// further down
			if ((ads->projectDirLock == 0) || Stricmp(s, recentmenuSnip[0].nm_Label))
			{
				// Even if we want it the projecttree might not exists
				// anymore, and then we should remove it from the list
				UBYTE offset = *(s - 1);
				BPTR testLock = Lock(s+offset, SHARED_LOCK);
	
				if (testLock)
				{
					i++;
					UnLock(testLock);
				}
			}
			
			s = end;
		}
		Close(file);
	}

	if (!i)
	{
		recentmenuSnip[i].nm_Type = NM_SUB;
		recentmenuSnip[i].nm_Label = "No Recent projecttrees";
		recentmenuSnip[i].nm_Flags = ITEMENABLED;
		i++;
	}

	recentmenuSnip[i].nm_Type = NM_END;

	ads->recentMenuItems = (struct MenuItem *)CreateMenusA(recentmenuSnip, NULL);
	ads->menu->FirstItem->NextItem->NextItem->
		NextItem->NextItem->NextItem->NextItem->NextItem->
		SubItem = ads->recentMenuItems;
	if  (ads->visualInfo) // ie not first time around
		LayoutMenuItems(ads->menu->FirstItem, ads->visualInfo, GTMN_NewLookMenus, TRUE, TAG_DONE);

	SetMenuStrip(ads->window, windowMenu);

	if (ads->projectDirLock && (file = Open("ENVARC:Codecraft/recent", MODE_NEWFILE)))
	{
		struct MenuItem *item = ads->recentMenuItems;
		while (item)
		{
			FWrite(file, ((struct IntuiText *)item->ItemFill)->IText - 1, 1, 1);
			FPuts(file, ((struct IntuiText *)item->ItemFill)->IText);
			FPutC(file, '\n');

			item = item->NextItem;
		}
		Close(file);
	}
}

void openRecentProjectTree(struct Codecraft *ads, UWORD index)
{
	struct MenuItem *item = ads->recentMenuItems;
	while (item && index--)
		item = item->NextItem;

	if (item)
	{
		UBYTE offset = *(((struct IntuiText *)item->ItemFill)->IText - 1);
		openProjectTree(ads, ((struct IntuiText *)item->ItemFill)->IText + offset);
	}
	ads->doRecentMenu = -1;
}

static BOOL actionRecent0(void)
{
	myCodecraft.doRecentMenu = 0;
	return FALSE;
}

static BOOL actionRecent1(void)
{
	myCodecraft.doRecentMenu = 1;
	return FALSE;
}

static BOOL actionRecent2(void)
{
	myCodecraft.doRecentMenu = 2;
	return FALSE;
}

static BOOL actionRecent3(void)
{
	myCodecraft.doRecentMenu = 3;
	return FALSE;
}

static BOOL actionRecent4(void)
{
	myCodecraft.doRecentMenu = 4;
	return FALSE;
}

static BOOL actionRecent5(void)
{
	myCodecraft.doRecentMenu = 5;
	return FALSE;
}

static BOOL actionRecent6(void)
{
	myCodecraft.doRecentMenu = 6;
	return FALSE;
}


static BOOL actionToggleBreakpoint(void)
{
    struct Codecraft *ads = &myCodecraft;

    BreakPointsToggleAtBlockNum(ads->currentAdsDocument, GetBlockNum(ads));
    ads->tei->redrawLeftBar(ads->currentAdsDocument->document);

    return FALSE;
}

static BOOL actionDeleteAllBookmarks(void)
{
	struct Codecraft *ads = &myCodecraft;

	deleteAllBreakPoints(ads);
    ads->tei->redrawLeftBar(ads->currentAdsDocument->document);

	return FALSE;
}

static BOOL actionDisableAllBookmarks(void)
{
	struct Codecraft *ads = &myCodecraft;

	setDisableAllBreakPoints(ads, TRUE);
    ads->tei->redrawLeftBar(ads->currentAdsDocument->document);
	return FALSE;
}

static BOOL actionEnableAllBookmarks(void)
{
	struct Codecraft *ads = &myCodecraft;

	setDisableAllBreakPoints(ads, FALSE);
    ads->tei->redrawLeftBar(ads->currentAdsDocument->document);

	return FALSE;
}

static BOOL	actionPrevMessage(void)
{
	struct Codecraft *ads = &myCodecraft;

	gotoPrevMessage(ads);

	return FALSE;
}

static BOOL	actionNextMessage(void)
{
	struct Codecraft *ads = &myCodecraft;

	gotoNextMessage(ads);

	return FALSE;
}

BOOL __ASM__ handlekey(__REG__(a0, struct Extension *ext), __REG__(a1, struct IntuiMessage *msg))
{
	struct Codecraft *ads = &myCodecraft;

    if (msg->Code == RAWKEY_F9)
        actionToggleBreakpoint();
    else if (msg->Code == RAWKEY_F6)
    {
   		if (ads->debugger->debuggerMode == NOTACTIVE)
   		{
	   		if (msg->Qualifier & IEQUALIFIER_CONTROL)
				actionRebuildProjectTree();
			else if (msg->Qualifier & IEQUALIFIER_SHIFT)
			{
				if (ads->buildInProgress)
				{
				}
			}
			else
		        actionBuildProjectTree();
	    }
	}
	else if (msg->Code == RAWKEY_F8 && (msg->Qualifier & IEQUALIFIER_SHIFT))
		actionPrevMessage();
	else if (msg->Code == RAWKEY_F8)
		actionNextMessage();
	else if (msg->Code == RAWKEY_F5 && (msg->Qualifier & IEQUALIFIER_CONTROL))
	{
		if (ads->debugger->debuggerMode == NOTACTIVE)
			actionRunNoDebug();
	}
	else if (msg->Code == RAWKEY_F5 && (msg->Qualifier & IEQUALIFIER_SHIFT))
	{
		if (ads->debugger->debuggerMode == BROKEN)
			actionStopDebug();
	}
	else if (msg->Code == RAWKEY_F5)
	{
		if (ads->debugger->debuggerMode == NOTACTIVE)
			actionRunDebug();
		if (ads->debugger->debuggerMode == BROKEN)
			actionResumeDebug();

	}
	else if (msg->Code == RAWKEY_F10)
	{
		ULONG qual =
		    (msg->Qualifier & (IEQUALIFIER_SHIFT | IEQUALIFIER_CONTROL));
		if (ads->debugger->debuggerMode == BROKEN)
		{
			if (qual == IEQUALIFIER_CONTROL)
				actionStepIntoDebug();
			else if (qual & IEQUALIFIER_SHIFT)
				actionStepOutDebug();
			else
				actionStepOverDebug();
		}

	}
	return TRUE;
}


static BOOL actionNewProjectTree(void)
{
	newProjectTree(&myCodecraft);
	return FALSE;
}

static BOOL actionOpenProjectTree(void)
{
	openProjectTree(&myCodecraft, NULL);
	updateRecentMenu(&myCodecraft);

    return FALSE;
}

void saveAll(struct Codecraft *ads)
{
	struct CodecraftDoc *adsDoc = (struct CodecraftDoc *)ads->documentList.mlh_Head;

	while (adsDoc->node.mln_Succ)
	{
		ULONG changed = (ULONG)ads->tei->executeRexxCommand(adsDoc->document, "GETATTR PROJECT CHANGED");

		if (changed)
			ads->tei->executeRexxCommand(adsDoc->document, "SAVE\n");
		adsDoc = (struct CodecraftDoc *)adsDoc->node.mln_Succ;
	}
	saveProjectTree(ads);
}

static BOOL actionSaveAll(void)
{
	saveAll(&myCodecraft);
	return FALSE;
}

static BOOL actionParameters(void)
{
	editProjectTreeParameters(&myCodecraft);
	return FALSE;
}

static BOOL actionBuildProjectTree(void)
{
	if (myCodecraft.buildInProgress)
		return FALSE;
	
	saveAll(&myCodecraft);
	buildProjectTree(&myCodecraft, myCodecraft.project.buildCmd, TRUE);
	return FALSE;
}

static BOOL actionCleanProjectTree(void)
{
	if (myCodecraft.buildInProgress)
		return FALSE;
	
	saveAll(&myCodecraft);
	buildProjectTree(&myCodecraft, myCodecraft.project.cleanCmd, TRUE);
	return FALSE;
}

static BOOL actionRebuildProjectTree(void)
{
	if (myCodecraft.buildInProgress)
		return FALSE;
	
	saveAll(&myCodecraft);
	buildProjectTree(&myCodecraft, myCodecraft.project.cleanCmd, TRUE);
	
	myCodecraft.FollowupAfterBuild = 1;
	return FALSE;
}

static BOOL actionRunDebug(void)
{
	if (myCodecraft.buildInProgress)
		return FALSE;
	
	runProject(&myCodecraft, TRUE);
	return FALSE;
}

static BOOL actionRunNoDebug(void)
{
	if (myCodecraft.buildInProgress)
		return FALSE;
	
	runProject(&myCodecraft, FALSE);
	return FALSE;
}

static BOOL actionBreakDebug(void)
{
	BreakDebug(myCodecraft.debugger);
	return FALSE;
}

static BOOL actionResumeDebug(void)
{
	ResumeDebug(myCodecraft.debugger);
	return FALSE;
}

static BOOL actionStopDebug(void)
{
	StopDebug(myCodecraft.debugger);
	return FALSE;
}

static BOOL actionStepIntoDebug(void)
{
	StepIntoDebug(myCodecraft.debugger);
	return FALSE;
}

static BOOL actionStepOverDebug(void)
{
	StepOverDebug(myCodecraft.debugger);
	return FALSE;
}

static BOOL actionStepOutDebug(void)
{
	StepOutDebug(myCodecraft.debugger);
	return FALSE;
}

static BOOL actionOnBrkDont(void)
{
	myCodecraft.debugger->onBreakShow = 0;
	return FALSE;
}

static BOOL actionOnBrkVar(void)
{
	myCodecraft.debugger->onBreakShow = 1;
	return FALSE;
}

static BOOL actionOnBrkCall(void)
{
	myCodecraft.debugger->onBreakShow = 2;
	return FALSE;
}

void UpdateMenuStates(struct Codecraft *ads, BOOL gr1, BOOL gr2, BOOL gr3)
{
	if (gr1)
	{
		ItemAddress(ads->menu, FULLMENUNUM(1, 0, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(1, 1, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(1, 2, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 0, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 1, 0))->Flags |= ITEMENABLED;
	}
	else
	{
		ItemAddress(ads->menu, FULLMENUNUM(1, 0, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(1, 1, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(1, 2, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 0, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 1, 0))->Flags &= ~ITEMENABLED;
	}
	
	if (gr2)
		ItemAddress(ads->menu, FULLMENUNUM(2, 2, 0))->Flags |= ITEMENABLED;
	else
		ItemAddress(ads->menu, FULLMENUNUM(2, 2, 0))->Flags &= ~ITEMENABLED;

	if (gr3)
	{
		ItemAddress(ads->menu, FULLMENUNUM(2, 4, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 5, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 6, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 7, 0))->Flags |= ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 8, 0))->Flags |= ITEMENABLED;
	}
	else
	{
		ItemAddress(ads->menu, FULLMENUNUM(2, 4, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 5, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 6, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 7, 0))->Flags &= ~ITEMENABLED;
		ItemAddress(ads->menu, FULLMENUNUM(2, 8, 0))->Flags &= ~ITEMENABLED;
	}
}
