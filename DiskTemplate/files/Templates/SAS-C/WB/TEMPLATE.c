#define __CLIB_PRAGMA_LIBCALL
#include <stdio.h>
#include <string.h>
#include <proto/exec.h>
#include <proto/dos.h>


char *vers="\0$VER: %TEMPLATE% (dd.mm.yyyy)";

#define TEMPLATE       "FANCY/K,FILE/M"
#define OPT_FANCY 0
#define OPT_FILENAME 1
#define OPT_COUNT 2


int main(int argc, char **argv)
{
	struct RDArgs *rdargs = NULL;
	LONG opts[OPT_COUNT];

	memset(opts, 0, sizeof(opts));

	printf("%TEMPLATE%\n");
	
	if (rdargs = ReadArgs(TEMPLATE, opts, NULL))
	{
		if (opts[OPT_FANCY])
			 printf("you selected fancy: %s\n", (STRPTR)opts[OPT_FANCY]);

		if (opts[OPT_FILENAME])
		{
			STRPTR *filenames = (STRPTR *)opts[OPT_FILENAME];
			while (*filenames)
				printf("on file: %s\n", *(filenames++));
		}
	}
	return 0;
}

